import requests
import json
import shutil
import base64
import functools
import websockets
import asyncio
import ssl
import pathlib

# Create a websocket client
# Connect to this server
#

async def ws_start(websocket, path, photo_base_url, api_key, img_save_path, query_tag):
    client_command = await websocket.recv()

    # get the image save path (path + image_name)
    img_name = img_download(photo_base_url, api_key, img_save_path, query_tag)

    with open(img_name, "rb") as img:
        encoded_img = base64.b64encode(img.read())
    img = encoded_img
    await websocket.send(img_name)
    await websocket.send(img)



def jprint(obj):
    text = json.dumps(obj, sort_keys=True, indent=4)
    print(text)

def img_download(PHOTO_BASE_URL, API_KEY, img_save_path, query_tag):
    pexels_header = {'Authorization': API_KEY}
    query = {'query': query_tag}

    response = requests.get(PHOTO_BASE + "/search?", headers=pexels_header, params=query)
    print(f"Response Code: {response.status_code}")

    # if the connection is established
    if 200 == response.status_code:
        i_response = json.loads(response.text)
        print(i_response["photos"])
        print(f"Number of photos: {len(i_response['photos'])}")
        print(i_response["photos"][0]["id"])
        photo_id = i_response["photos"][0]["id"]

        photo_response = requests.get(PHOTO_BASE + "/photos/" + str(photo_id), headers=pexels_header)
        print(f"Request for Photo: {photo_response.status_code}")

        jprint(photo_response.json())
        p_response = json.loads(photo_response.text)
        print(f"Photo Response: {p_response['src']['original']}")
        img_url = p_response['src']['original']
        img_name = img_url.split("/")[-1]
        img_name = img_save_path + img_name

        img_request = requests.get(img_url, stream=True)
        print(f"Image Request Code: {img_request.status_code}")

        # download the image
        if 200 == img_request.status_code:
            img_request.raw_decode_content = True

            with open(img_name, 'wb') as f:
                shutil.copyfileobj(img_request.raw, f)

            print(f"Image downloaded: {img_name}")

            return img_name


if __name__ == '__main__':
    PHOTO_BASE = "https://api.pexels.com/v1"
    VIDEO_BASE = "https://api.pexels.com/videos"
    API_KEY = ""
    img_save_path = r"D:\Workspace\bottle\downloaded_images\\"
    query_tag = "cars"

    # Download the first image when searching for 'people'
    # img_path = img_download(PHOTO_BASE, API_KEY, img_save_path, query_tag)
    # print(f"Complete path returned: {img_path}")

    # Start the server
    ssl_context = ssl.SSLContext(ssl.PROTOCOL_TLS_SERVER)
    localhost_pem = pathlib.Path(__file__).with_name("localhost.pem")
    ssl_context.load_cert_chain(localhost_pem)

    bound_handler = functools.partial(ws_start, photo_base_url = PHOTO_BASE, api_key = API_KEY, \
                                      img_save_path = img_save_path, query_tag = query_tag)
    start_server = websockets.serve(bound_handler, "localhost", 8765, ssl=ssl_context)
    print("[*] HTTPS Webserver started at localhost:8765")
    asyncio.get_event_loop().run_until_complete(start_server)
    asyncio.get_event_loop().run_forever()

    # Websockets handler
    #   1. Websocket reconnect handler (in case of unepxected disconnect)
    #   2. Websocket close
    #   3. Messege Handler
