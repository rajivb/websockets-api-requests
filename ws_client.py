import asyncio
import pathlib
import ssl
import websockets

ssl_context = ssl.SSLContext(ssl.PROTOCOL_TLS_CLIENT)
localhost_pem = pathlib.Path(__file__).with_name("localhost.pem")
ssl_context.load_verify_locations(localhost_pem)


async def ws_client(url, port):
    uri = "wss://" + url + ":" + str(port)

    async with websockets.connect(uri, ssl=ssl_context) as websocket:
        command = input("Code: ")
        await websocket.send(command)
        print(f"Sent {command}")

        # receive the image location and name
        webserver_response = await websocket.recv()
        print(f"[1] Received from Webserver: {webserver_response}")


        try:
            webserver_response = await websocket.recv()
        except:
            webserver_response = await websockets.connect(uri, ssl=ssl_context)
        print(f"[2] Received from Webserver: {webserver_response}")

if __name__ == '__main__':
    asyncio.get_event_loop().run_until_complete(ws_client("localhost", 8765))